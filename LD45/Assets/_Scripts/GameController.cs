﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	[SerializeField] private GameObject _mFinalPart;
	[SerializeField] private Image _mFinalBackground;
	[SerializeField] private Image _mFinalPhoto;
	[SerializeField] private Button _mContinueButton;

	public bool IsGameEnded;

	private YieldInstruction fadeInstruction = new YieldInstruction();

	void Awake()
	{
		_mContinueButton.onClick.AddListener(LoadMainMenu);
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	public void EndGame()
	{
		IsGameEnded = true;
		_mFinalPart.SetActive(true);
		StartCoroutine(FadeIn(_mFinalBackground));
		StartCoroutine(FadeIn(_mFinalPhoto));
	}

	private void LoadMainMenu()
	{
		SceneManager.LoadScene("MainMenuScene");
	}

	IEnumerator FadeIn(Image image)
	{
		var elapsedTime = 0.0f;
		var c = image.color;
		while (elapsedTime < 5f)
		{
			yield return fadeInstruction;
			elapsedTime += Time.deltaTime;
			c.a = Mathf.Clamp01(elapsedTime / 5f);
			image.color = c;
		}
		_mContinueButton.gameObject.SetActive(true);
	}
}
