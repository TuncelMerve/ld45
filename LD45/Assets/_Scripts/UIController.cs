﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

	[SerializeField] private Button _mStart = null;
	[SerializeField] private Button _mCredits = null;
	[SerializeField] private Button _mExit = null;
	[SerializeField] private Button _mCreditsBack = null;

	[SerializeField] private GameObject _mAboutPanel;

	[SerializeField] private AudioSource _mButtonSound;

	private void Start()
	{
		Debug.Assert(_mStart != null);
		Debug.Assert(_mCredits != null);
		Debug.Assert(_mExit != null);
		Debug.Assert(_mCreditsBack != null);

		_mStart.onClick.AddListener(StartGame);
		_mCredits.onClick.AddListener(ShowCredits);
		_mExit.onClick.AddListener(Exit);
		_mCreditsBack.onClick.AddListener(Back);
	}

	private void StartGame()
	{
		_mButtonSound.Play();
		SceneManager.LoadScene("MainScene");
	}

	private void ShowCredits()
	{
		_mButtonSound.Play();
		_mAboutPanel.SetActive(true);

		var graphicsComponents = _mAboutPanel.GetComponentsInChildren<Graphic>();

		foreach (var component in graphicsComponents)
		{
			GraphicExtensions.FadeIn(component, 0.5f);
		}

		GraphicExtensions.FadeIn(_mAboutPanel.GetComponent<Graphic>(), 0.5f);
	}

	private void Exit()
	{
		_mButtonSound.Play();
		Application.Quit();
	}

	private void Back()
	{
		_mButtonSound.Play();

		var graphicsComponents = _mAboutPanel.GetComponentsInChildren<Graphic>();

		foreach (var component in graphicsComponents)
		{
			GraphicExtensions.FadeOut(component);
		}

		GraphicExtensions.FadeOut(_mAboutPanel.GetComponent<Graphic>());

		Invoke("HideAboutPanel", 0.3f);
	}

	private void HideAboutPanel()
	{
		_mAboutPanel.SetActive(false);
	}
}

public static class GraphicExtensions
{
	public static void FadeIn(Graphic g, float time = 2f)
	{
		g.GetComponent<CanvasRenderer>().SetAlpha(0f);
		g.CrossFadeAlpha(1f, time, false);
	}

	public static void FadeOut(Graphic g)
	{
		g.GetComponent<CanvasRenderer>().SetAlpha(1f);
		g.CrossFadeAlpha(0f, .3f, false);
	}
}

