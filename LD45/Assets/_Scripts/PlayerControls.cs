﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
	[SerializeField] private Transform _mLady;

	private const int TileSize = 2;
	private const int TileMin = -6;
	private const int TileMax = 6;

	private bool _mLerp;
	private Vector3 _mTargetPosition;
	private GameController _mGameController;

	void Start()
	{
		_mGameController = FindObjectOfType<GameController>();
	}

	void Update()
	{
		if (_mGameController.IsGameEnded)
		{
			return;
		}

		//if lady is on the air can't switch lanes
		if(_mLady.localPosition.y > -1) return;

		var pos = gameObject.transform.position;

		//if lady reached the target stop the lerp
		if (_mLerp && Vector3.Distance(pos, _mTargetPosition) < 0.01f)
		{
			_mLerp = false;
		}

		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			var newX = Mathf.Clamp(pos.x - TileSize, TileMin, TileMax);

			_mTargetPosition = new Vector3(newX, pos.y, pos.z);
			_mLerp = true;
		}
		else if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			var newX = Mathf.Clamp(pos.x + TileSize, TileMin, TileMax);

			_mTargetPosition = new Vector3(newX, pos.y, pos.z);
			_mLerp = true;
		}

		if (_mLerp)
		{
			gameObject.transform.position = Vector3.Lerp(pos, _mTargetPosition, Time.deltaTime * 2f);
		}
			
	}
}
