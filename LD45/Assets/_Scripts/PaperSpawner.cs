﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperSpawner : MonoBehaviour {

	[SerializeField] private AudioSource _mWindSound;
	[SerializeField] private List<AudioSource> _mStormMoanSounds;
	[SerializeField] private List<AudioSource> _mJumpSounds;
	[SerializeField] private List<AudioSource> _mLandSounds;
	[SerializeField] private AudioSource _mBreatheSound;
	[SerializeField] private ParticleSystem _mWindParticles;

	private ParticleSystem _mPart;
	private GameController _mGameController;

	void Start()
	{
		_mGameController = FindObjectOfType<GameController>();
		_mPart = GetComponent<ParticleSystem>();
		Invoke("Wind", 10f);
		Invoke("JumpSound", 0.2f);
		Invoke("LandSound", 4f);
		Invoke("BreatheSound", 4.5f);
	}

	void Wind()
	{
		if (_mGameController.IsGameEnded) return;

		_mWindSound.Play();
		_mWindParticles.Play();

		var p = new ParticleSystem.Particle[_mPart.particleCount + 1];
		var particles = _mPart.GetParticles(p);

		var i = 0;
		while (i < particles)
		{
			p[i].velocity = new Vector3(p[i].remainingLifetime / p[i].startLifetime * 15F, p[i].remainingLifetime / p[i].startLifetime * 15F, 0);
			i++;
		}

		_mPart.SetParticles(p, particles);

		_mStormMoanSounds[Random.Range(0, _mStormMoanSounds.Count)].Play();

		var randomTime = Random.Range(20f, 25f);
		Invoke("Wind", randomTime);
	}

	void JumpSound()
	{
		if (_mGameController.IsGameEnded) return;

		_mJumpSounds[Random.Range(0, _mJumpSounds.Count)].Play();

		Invoke("JumpSound", 6f);
	}

	void LandSound()
	{
		if (_mGameController.IsGameEnded) return;

		_mLandSounds[Random.Range(0, _mLandSounds.Count)].Play();

		Invoke("LandSound", 6f);
	}

	void BreatheSound()
	{
		if (_mGameController.IsGameEnded) return;

		_mBreatheSound.Play();

		Invoke("BreatheSound", 6f);
	}
}
