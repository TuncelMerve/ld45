﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class Memories : MonoBehaviour
{
	[SerializeField] private TMP_Text _mScoreText;
	[SerializeField] private TMP_Text _mMemoryText;

	[SerializeField] private List<AudioSource> _mCollectSounds;
	[SerializeField] private AudioSource _mLayerOne;
	[SerializeField] private AudioSource _mLayerTwo;
	[SerializeField] private AudioSource _mLayerThree;

	private ParticleSystem _mPart;
	private GameController _mGameController;
	private List<ParticleCollisionEvent> _mCollisionEvents;

	private YieldInstruction fadeInstruction = new YieldInstruction();

	private int _mCurrentMemory;

	private readonly List<string> _mMemories = new List<string>
	{
		"Where is he?",
		"I'm remembering some things",
		"How long has it been since?",
		"Was it yesterday or last year?",
		"that we were sitting in the living room",
		"smiling at each other",
		"Did I feed the cat?",
		"maybe he did",
		"she looks hungry",
		"and lonely",
		"I think this was...",
		"Wait! Do I remember that right?",
		"When was the last time we...?",
		"It is blurry",
		"Yes yes... it was at the party",
		"Oh I remember now",
		"Remembering what he had said about...",
		"the lovely evening",
		"it is not a bad thing to remember...",
		"...that he is gone",
		"quite a while",
		"I keep coming back to him in my head",
		"coming back to the emotions",
		"collecting my memories",
		"it does not hurt",
		"I am glad",
		"that I have him in my head",
		"going back",
		"This song...",
		"...makes me remember...",
		"...the way he smiles",
		"...the way he loves",
		"smiled...loved...",
		"I know that's what people say",
		"you'll get over it",
		"but at least...",
		"I remember now",
		"I love these random memories",
		"Memories are all that I have left"

	};

	private const int MaxScore = 40;

	private int _mScore;

	void Start()
	{
		_mPart = GetComponent<ParticleSystem>();
		_mGameController = FindObjectOfType<GameController>();
		_mCollisionEvents = new List<ParticleCollisionEvent>();

		_mScoreText.text = String.Format("{0}/{1}", 0f, MaxScore);
	}

	void Update()
	{
		if (_mGameController.IsGameEnded)
		{
			_mPart.Stop();
		}
	}

	void OnParticleCollision(GameObject other)
	{
		var numCollisionEvents = _mPart.GetCollisionEvents(other, _mCollisionEvents);

		var rb = other.GetComponent<Rigidbody2D>();
		var i = 0;

		while (i < numCollisionEvents)
		{
			if (rb)
			{
				//var pos = _mCollisionEvents[i].intersection;
				//var force = _mCollisionEvents[i].velocity * 10;
				//rb.AddForce(force);

				//if (_mScore % 3 == 0)
				//{
				StartCoroutine(ShowNextMemory());
				//}

				_mScore++;

				if (_mScore >= (MaxScore / 4))
				{
					_mLayerOne.volume = Mathf.Clamp(_mLayerOne.volume + 0.1f, 0f, 0.7f);
				}
				if (_mScore >= ((MaxScore / 4) * 2))
				{
					_mLayerTwo.volume = Mathf.Clamp(_mLayerTwo.volume + 0.1f, 0f, 0.6f);
				}
				if(_mScore >= ((MaxScore / 4) * 3))
				{
					_mLayerThree.volume = Mathf.Clamp(_mLayerThree.volume + 0.1f, 0f, 0.7f);
				}

				_mCollectSounds[Random.Range(0, _mCollectSounds.Count)].Play();
				_mScoreText.text = String.Format("{0}/{1}", _mScore, MaxScore);

				if (_mScore == MaxScore)
				{
					_mGameController.EndGame();
				}
				
			}
			i++;			
		}
	}

	IEnumerator ShowNextMemory()
	{
		if (_mCurrentMemory == _mMemories.Count)
		{
			yield return true;
		}
		
		_mMemoryText.text = _mMemories[_mCurrentMemory];
		_mCurrentMemory++;

		var c = _mMemoryText.color;
		var elapsedTime = 0.0f;

		c.a = 0;
		_mMemoryText.color = c;

		while (elapsedTime < 5f)
		{
			yield return fadeInstruction;
			elapsedTime += Time.deltaTime;
			c.a = Mathf.Clamp01(elapsedTime / 5f);
			_mMemoryText.color = c;
		}

	}

}
